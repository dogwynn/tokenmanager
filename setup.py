from pathlib import Path

# http://stackoverflow.com/a/27868004/1869370
# from distutils.core import setup
from setuptools import setup

here = Path(__file__).resolve().parent

long_description = Path(here,'README.rst').resolve().read_text()

setup(
    name = 'tokenmanager',
    packages = ['tokenmanager'],
    install_requires=[
        'ruamel.yaml<0.15',
        'PyYAML',               # Need because gems does
        'gems',
    ],

    version = '0.0.1',
    description = 'Simple token manager for token-based REST APIs',
    long_description = long_description,
    url = 'https://bitbucket.org/dogwynn/tokenmanager',

    author="David O'Gwynn",
    author_email="dogywnn@acm.org",
    license='BSD',

    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python :: 3',
    ],
    keywords='token manager rest api development helper utility',
)
